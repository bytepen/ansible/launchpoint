# Ansible for GCP LaunchPoint+

Scripts to facilitate setting up various things on Synack's GCP LaunchPoint+ 

## Usage

If you want to use the `./playbooks/_default.yml` playbook or you have already created a playbook base on your username (such as `./playbooks/reqsocl11b.yml`), you can simply run the following:

```
sudo apt install -y git
git clone https://gitlab.com/bytepen/ansible/launchpoint.git
cd launchpoint
./setup.sh
```

If you wish to install someone else's playbook, you can run something like the following:

```
./setup.sh reqsocl11b
```

You can install one specific tool with a command like the following:

```
./install_tool.sh kxss
./install_tool.sh gobuster
```


## Role Customization & Contribution

The default roles installed are the ones I like, but it is likely that you will have different tools you like.

I have opted to take the Forking strategy of the QMK firmware, which can be read about [here](https://beta.docs.qmk.fm/using-qmk/guides/git-best-practices/newbs_git_using_your_master_branch).
The benefit to this strategy is that it easily allows everyone to contribute this project and share their customizations without stepping on the toes of anyone else.

As a basic summary, you should fork this repository onto your GitLab repositories.
Once forked, you should create a new branch named whatever you like; perhaps `custom-{srt_username}`.
Inside of that branch, you should create a file inside of `./playbooks` that matches your SRT username; for example, mine would be `./playbooks/reqsocl11b.yml`.
If a file matching your username doesn't exist, `./playbooks/_default.yml` will be used instead.
Once you have it working the way you like, you should make a PR from your `custom-{srt_username}` branch to my `main` branch.

Once I accept it, anyone who wishes to have the same setup as you would be able to launch the setup script as described in the `Usage` section above.

You might be asking why you should create a custom branch on your fork.
The reason for this is to allow you to easily and cleanly pull updates from the original repository.

For example, these are the steps you would take to update:

```
git clone git@gitlab.com:{you}/launchpoint.git
git remote add upstream git@gitlab.com:bytepen/ansible/launchpoint.git
git checkout main
git fetch upstream
git pull upstream main
git push 
git checkout custom-{srt_username}
git rebase main
```

From there, you can continue to work on your `custom-{srt_username}` branch while being able to use the roles others have created.

### Role Naming Conventions

I'm really hoping that others will buy into crowd sourcing this and many people contribute.
With that, I figured it would be wise to create a defined naming scheme for roles right out of the gate.

Given how easy it is to add new roles, each role should have exactly one clear purpose.
They should start with the target operating system, followed by the type of role, followed by the commonly known name.
If there is a paid and free version of a piece of software, it should exist as Burp Suite does in the example below.

For example, here would be some good naming choices:
```
ubuntu/customizations/synack_wallpaper
ubuntu/customizations/zsh
ubuntu/environments/i3wm
ubuntu/tools/burp_suite/ce
ubuntu/tools/burp_suite/pro
ubuntu/tools/docker
```

So far, I'm defining the role types as:
* Customizations: Small tweaks that change how LP+ works/looks
* Environments: Desktop Environments - These are very big changes. Make sure you know what you are doing
* Frameworks: Software that other tools may depend on
* Tools: Software you would typically interact with

If you'd like to see additional role types or have other ideas that would help you contribute, please feel free to let me know via the Synack slack.

## Current Roles

Here's a list of current roles, what they do, and if they're installed as part of the `_default.yml` playbook:

| Default | OS | Type | Role | Description 
| -- | -- | -- | -- | --
|   | ubuntu | customizations | nopasswd_sudo         | Removes the requirement to enter the password when you use sudo
| X | ubuntu | customizations | synack_wallpaper      | Adds the Synack Wallpaper
|   | ubuntu | customizations | zsh                   | Installs zsh, oh-my-zsh, and sets zsh to your default shell
|   | ubuntu | environments   | i3wm                  | Installs i3 Window Manager and related utils
|   | ubuntu | environments   | mate                  | Installs the Mate Desktop Manager
| X | ubuntu | frameworks     | docker                | Installs Docker and its APT Repos
| X | ubuntu | frameworks     | golang                | Install GoLang
|   | ubuntu | frameworks     | nodejs                | Install Node.js and Yarn
| X | ubuntu | tools          | amass                 | OWASP's attack surface/network mapper
|   | ubuntu | tools          | anew                  | Tool to append unique/new items to a file
| X | ubuntu | tools          | aquatone              | Tool for visual inspection of websites enmass
|   | ubuntu | tools          | assetfinder           | Finds related domains and subdomains
| X | ubuntu | tools          | burp_suite/ce         | Installs Burp Suite CE
|   | ubuntu | tools          | burp_suite/pro        | Installs Burp Suite Pro
|   | ubuntu | tools          | chaos                 | Allows interaction with ProjectDiscovery Chaos API
| X | ubuntu | tools          | common_cli_utils      | Installs some common CLI utilities, such as `curl`, `tmux`, and `vim`
| X | ubuntu | tools          | dalfox                | XSS Scanner
| X | ubuntu | tools          | dnsx                  | Multipurpose DNS toolkit
| X | ubuntu | tools          | ffuf                  | Fast web fuzzer
| X | ubuntu | tools          | flameshot             | Install Flameshot Screenshot utility
| X | ubuntu | tools          | getallurls            | Scrapes various sources for URLs related to a domain
|   | ubuntu | tools          | getjs                 | Extracts JavaScript files from given URLs
|   | ubuntu | tools          | gf                    | A wrapper around grep to avoid typing common patterns
| X | ubuntu | tools          | gobuster              | Tool to brute force web directories, subdomains, and vhosts
|   | ubuntu | tools          | google_chrome         | Installs Google Chrome
|   | ubuntu | tools          | gowitness             | Tool to screenshot websites using Chrome Headless
| X | ubuntu | tools          | hawkrawler            | Web crawler for gathering URLs and JavaScript locations
|   | ubuntu | tools          | httprobe              | Probes domains for working http(s) servers
| X | ubuntu | tools          | httpx                 | Multipurpose HTTP Toolkit to probe web servers
| X | ubuntu | tools          | kxss                  | Emoe's fork of tomnomnom's kxss hacks. Checks URL params for returned data
| X | ubuntu | tools          | libreoffice           | Office Suite
| X | ubuntu | tools          | masscan               | Builds the latest masscan from source
| X | ubuntu | tools          | nmap                  | Builds the latest nmap from source
|   | ubuntu | tools          | nuclei                | Template based vulnerability scanner
| X | ubuntu | tools          | peek                  | Dead simple video recorder
| X | ubuntu | tools          | shuffledns            | Wrapper around massdns to enumerate subdomains
| X | ubuntu | tools          | sqlmap                | Automatic SQL Injection and Database Takeover Tool
|   | ubuntu | tools          | subgen                | Build a list of domains based on a wordlist and a domain
| X | ubuntu | tools          | subjack               | Subdomain takover tool
| X | ubuntu | tools          | synackapi             | API Client to interact with Synack's API
| X | ubuntu | tools          | urlprobe              | Returns HTTP response codes for a list of websites
| X | ubuntu | tools          | waybackurl            | Fetches URLs from the Wayback Machine for a given domain
