#!/bin/bash

main() {
    echo $@
    setup $@
    run_ansible $@
}

setup() {
    /usr/bin/clear

    head_1 "Setting Up"

    BASE_DIR="$(dirname $(realpath ${BASH_SOURCE[0]}))"
    SCRIPT="$(realpath ${BASH_SOURCE[0]})"

    if [ "$EUID" != "0" ]; then
        exec /usr/bin/sudo /bin/bash "${SCRIPT}" $@
    fi
}

run_ansible() {
    head_1 "Running Ansible"
    if [ -x "/usr/bin/dpkg" ]; then
        /usr/bin/dpkg -s ansible > /dev/null 2>&1
        if [[ "$?" -ne "0" ]]; then
            /usr/bin/apt update && /usr/bin/apt install ansible -y
        fi
    fi

    head_2 "Checking for $@"

    MAIN=$(/usr/bin/find "${BASE_DIR}/roles/" -iwholename "*/${1}/*main.yml" | head -1)

    if [[ ! -z "${MAIN}" ]]; then
        echo "Found: ${MAIN}"
        echo "Install? [y/N] "
        read ANS

        if [[ "${ANS}" == "y"* ]]; then
            head_2 "Installing ${1}"
            /usr/bin/grep "${SUDO_USER}" /etc/sudo* -R | grep "NOPASSWD" > /dev/null 2>&1
            
            if [[ "$?" -eq 0 ]]; then
                /usr/bin/sudo -u ${SUDO_USER} /usr/bin/ansible localhost -i localhost -m include_tasks -a "${MAIN}"
            else
                /usr/bin/sudo -u ${SUDO_USER} /usr/bin/ansible localhost -i localhost -m include_tasks -a "${MAIN}" --ask-become-pass
            fi
        else
            head_2 "Bailing"
        fi
    else
        head_2 "No role found matching ${1}"
    fi
}

head_1() {
    /usr/bin/echo -e "==============================\n$1\n=============================="
}

head_2() {
    /usr/bin/echo -e "-------------------\n$1\n-------------------"
}

main $@
